﻿using System.Collections.Generic;

namespace Paises.Modelos
{
    //classe paises para mais tarde gravar os dados dos paises 
    public class Paises
    {
        public int numPais { get; set; }

        public string name { get; set; }

        public List<string> topLevelDomain { get; set; }

        public string capital { get; set; }

        public string region { get; set; }
        
        public string subregion { get; set; }

        public int population { get; set; }

        public string area { get; set; }
       
        public List<Currency> currencies { get; set; }

        public string flag { get; set; }

    }
}
