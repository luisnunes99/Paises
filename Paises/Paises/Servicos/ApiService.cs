﻿namespace Paises.Servicos
{
    using Newtonsoft.Json;
using Modelos;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
    public class ApiService
    {
        public async Task<Response> GetPaises(string urlBase, string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                var response = await client.GetAsync(controller);

                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result
                    };
            }
                var pais = JsonConvert.DeserializeObject<List<Paises>>(result);

                return new Response
                {
                    IsSuccess = true,
                   Result = pais
                };

            }
            catch(Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                   Message = ex.Message
                };
            }
        }
        
    }

}
