﻿namespace Paises.Servicos
{
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.IO;
    using Modelos;
    using System.Windows.Forms;

    class DataService
    {
        //SQLiteConnection  para testar as conexoes 
        private SQLiteConnection connection;

        //SQLCommandn  para executar comandos sql
        private SQLiteCommand command;

        //Mandar mensagens
        private DialogService dialogService;

        //metodo para criar a bd local
        public DataService()
        {
            dialogService = new DialogService();

           //se a pasta Data nao existir vai ser criada 
            if (!Directory.Exists("Data")) 
            {
                Directory.CreateDirectory("Data");
            }

            //caminho e nome do ficheiro bd 
            var path = @"Data\BDPaises.sqlite";

            try
            {
                //SQLiteConnection  para criar uma nova conexao
                connection = new SQLiteConnection("Data Source=" + path);
                connection.Open();
                
                //comando sql para criar a tabela paises
                string sqlcommand = "create table if not exists pais(numpais integer primary key,name string(250),capital string(250),region string(250),subregion string(250),population int,area string(250),flag text)";

                //executa o comando sql
                command = new SQLiteCommand(sqlcommand, connection);

                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);
            }

            //se a pasta Data nao existir vai ser criada 
            if (!Directory.Exists("Data"))
            {
                Directory.CreateDirectory("Data");
            }

            //caminho e nome do ficheiro bd 
            var path2 = @"Data\BDPaises.sqlite";

            try
            {
                //SQLiteConnection  para criar uma nova conexao
                connection = new SQLiteConnection("Data Source=" + path2);
                connection.Open();

               //comando sql para criar a tabela moedas
                string sqlcommand = "create table if not exists moedas(nummoeda integer primary key autoincrement,code string(250),name string(250),symbol string(250),numpais integer, foreign key (numpais) references pais (numpais))";

                //executa o comando sql
                command = new SQLiteCommand(sqlcommand, connection);

                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);
            }

            //se a pasta Data nao existir vai ser criada 
            if (!Directory.Exists("Data"))
            {
                Directory.CreateDirectory("Data");
            }

            //caminho e nome do ficheiro bd 
            var path3 = @"Data\BDPaises.sqlite";

            try
            {
                //SQLiteConnection  para criar uma nova conexao
                connection = new SQLiteConnection("Data Source=" + path3);
                connection.Open();

                //comando sql para criar a tabela dominios
                string sqlcommand = "create table if not exists dominios(numdominio integer primary key autoincrement,topLevelDomain string(250),numpais integer,foreign key (numpais) references pais (numpais))";

                //executa o comando sql
                command = new SQLiteCommand(sqlcommand, connection);

                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro a criar a tabela ", e.Message);
            }

        }

        //metodo para gravar os dados na bd
        public void GravarDadosBD(List<Paises> Pais)
        {
            //gravar paises na bd local
            try
            {
                int numpais = 1;

                foreach (var pais in Pais)
                {
                    //comando sql para gravar os dados dos paises na bd local
                    string sql =
                       string.Format("insert into pais (numpais,name, capital, region,subregion,population,area,flag) values (@numpais,@name,@capital,@region,@subregion,@population,@area,@flag)");

                    
                    command = new SQLiteCommand(sql, connection);
                    //gravar os dados de cada campo do pais 
                    command.Parameters.AddWithValue("@numpais", numpais);
                    command.Parameters.AddWithValue("@name", pais.name);
                    command.Parameters.AddWithValue("@capital", pais.capital);
                    command.Parameters.AddWithValue("@region", pais.region);
                    command.Parameters.AddWithValue("@subregion", pais.subregion);
                    command.Parameters.AddWithValue("@population", pais.population);
                    command.Parameters.AddWithValue("@area", pais.area);
                    command.Parameters.AddWithValue("@flag", pais.flag);

                    command.ExecuteNonQuery();

                    //gravar moedas na bd local          
                    foreach (var moeda in pais.currencies)
                    {
                        //comando sql para gravar os dados das moedas na bd local
                        sql =
                          string.Format("insert into moedas (code, name, symbol,numpais) values (@code,@name,@symbol,@numpais)");

                        //gravar os dados de cada campo das moedas
                        command = new SQLiteCommand(sql, connection);

                        //se houver moedas sem codigo adiciona "sem codigo" a este campo para o programa nao rebentar 
                        if (moeda.code != null)
                        {
                            command.Parameters.AddWithValue("@code", moeda.code);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@code", "Sem código");
                        }

                        //se houver moedas sem nome adiciona "sem nome" a este campo para o programa nao rebentar 
                        if (moeda.name != null)
                        {
                            command.Parameters.AddWithValue("@name", moeda.name);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@name", "Sem nome");
                        }

                        //se houver moedas sem simbolo adiciona "sem moeda" a este campo para o programa nao rebentar 
                        if (moeda.symbol != null)
                        {
                            command.Parameters.AddWithValue("@symbol", moeda.symbol);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@symbol", "Sem simbolo");
                        }
                       
                        command.Parameters.AddWithValue("@numpais", numpais);

                        command.ExecuteNonQuery();
                    }

                    //gravar dominios na bd local      
                    foreach (var dominio in pais.topLevelDomain)
                    {
                        //comando sql para gravar os dados dos dominios na bd local
                        sql =
                          string.Format("insert into dominios (topLevelDomain,numpais) values (@topLevelDomain,@numpais)");

                        //gravar os dados de cada campo dos dominios 
                        command = new SQLiteCommand(sql, connection);

                        command.Parameters.AddWithValue("@topLevelDomain", dominio);
                        command.Parameters.AddWithValue("@numpais", numpais);

                        command.ExecuteNonQuery();
                    }
                    numpais++;
                }

                connection.Close();

            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);
            }
        }

        internal void ApagarDadosBD()
        {
            try
            {
                string sql = string.Format("delete from pais; delete from moedas; delete from dominios; update sqlite_sequence set seq = 0");
                command = new SQLiteCommand(sql, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro a apagar os dados da bd", e.Message);
            }
        }


        public List<Paises> MostrarDadosBD()
        {
            List<Paises> Pais = new List<Paises>();

            try
            {
                string sql = "select numpais,name, capital, region,subregion,population,area,flag from pais";

                command = new SQLiteCommand(sql, connection);

                //lê cada registo 
                SQLiteDataReader reader = command.ExecuteReader();

                while(reader.Read())
                {
                    Paises country = new Paises();
                    country.numPais = Convert.ToInt32(reader["numpais"]);
                    country.name = (string)reader["name"];
                    country.capital = (string)reader["capital"];
                    country.region = (string)reader["region"];
                    country.subregion = (string)reader["subregion"];
                    country.population = Convert.ToInt32(reader["population"]);
                    country.area = reader["area"].ToString();
                    country.flag = (string)reader["flag"];
                    country.currencies = MostrarDadosBD(Convert.ToInt32(reader["numpais"]));
                    country.topLevelDomain = MostrarDominiosBD(Convert.ToInt32(reader["numpais"]));
                    Pais.Add(country);
                    
                }
                connection.Close();

                return Pais;
            }

            catch(SQLiteException e)
            {
                dialogService.ShowMessage("Erro a ir buscar os paises.", e.Message);
                return null;
            }
        }

        //metodo para mostrar os dominios da bd 
        private List<string> MostrarDominiosBD(int numPais)
        {
            List<string> dominios = new List<string>();

            try
            {
                //comando sql para mostrar os dominios
                string sql = "select topLevelDomain from dominios where numpais =" + numPais;

                command = new SQLiteCommand(sql, connection);

                //le os dados da bd
                SQLiteDataReader reader = command.ExecuteReader();

                //mostrar os dados 
                while (reader.Read())
                {
                    dominios.Add(reader["topLevelDomain"].ToString());
                }
                return dominios;
            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro a carregar os dominios da bd", e.Message);

                return null;
            }
        }

        //metodo para mostrar os dados das moedas 
        private List<Currency> MostrarDadosBD(int numPais)
        {
            List<Currency> moedas = new List<Currency>();

            try
            {
                //comando sql para mostrar os dados das moedas
                string sql = "select code, name, symbol from moedas where numpais =" + numPais; 

                command = new SQLiteCommand(sql, connection);

                //lr os dados
                SQLiteDataReader reader = command.ExecuteReader();

                //mostrar os dados 
                while (reader.Read())
                {
                    moedas.Add(new Currency
                    {
                        code = (string)reader["code"],
                        name = (string)reader["name"],
                        symbol = (string)reader["symbol"],
                    });
                }
                return moedas;
            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro a carregar as moedas", e.Message);

                return null;
            }
        }
    }
}

