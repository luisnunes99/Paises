﻿namespace Paises
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;
    using Servicos;
    using System.Threading.Tasks;

    public partial class Form1 : Form
    {
        #region Atributos 
        private List<Paises> Pais;

        private List<Currency> Moedas;

        private NetworkService networkService;

        private ApiService apiService;

        private DialogService dialogService;

        private DataService dataService;

        #endregion

        public Form1()
        {
            InitializeComponent();

            //declarar servicos 
            networkService = new NetworkService();
            apiService = new ApiService();
            dialogService = new DialogService();
            dataService = new DataService();
            LoadPaises();
        }

        //carregamento dos paises
        private async void LoadPaises()
        {
            bool load;

            LabelResultado.Text = "A atualizar paises";

            var connection = networkService.CheckConnection();

            //se nao houver internet chama o metodo para carregar os paises atraves da bd local
            if (!connection.IsSuccess)
            {
                LoadLocalPaises();
                load = false; }

            //se houver internet chama o metodo para carregar os paises atraves da API
            else
            {
                await LoadApiPaises();
                load = true;
            }

            //se nao houver internet e nem bd local manda a mensagem 
            if (Pais.Count == 0)
            {
                LabelResultado.Text = "Nao há ligacao a internet e ainda não foram carregados os dados";

                return;
            }

            //carregar a combobox com os nomes dos paises
            comboBoxPais.DataSource = Pais;
            comboBoxPais.DisplayMember = "name";

            LabelResultado.Text = "Paises Carregados";

            //se os paises forem carregados pela internet mostra a mensagem
            if (load)
            {
                LabelResultado.Text = string.Format("Paises carregados da internet em {0:F}", DateTime.Now);
            }
            //se os paises forem carregados atraves da bd local mostra a mensagem 
            else
            {
                LabelResultado.Text = string.Format("Paises carregados da BD em {0:F}",DateTime.Now);
            }

            //preenche a progress bar
            progressBar1.Value = 100;
        }

        //metodo para carregar os paises atraves ba bd local 
        private void LoadLocalPaises()
        {
            Pais = dataService.MostrarDadosBD();
        }

        //metodo para carregar os paises atraves da API
        private async Task LoadApiPaises()
        {
            progressBar1.Value = 0;

            LabelResultado.Text = "A carregar paises.";
            LabelResultado.Update();

            var response = await apiService.GetPaises("https://restcountries.eu", "/rest/v2/all");

            //transforma o objecto pais para uma lista
            Pais = (List<Paises>)response.Result;

            //chama o metodo que apaga os dados da bd local
            dataService.ApagarDadosBD();
            //chama o metodo que grava os dados da bd local
            dataService.GravarDadosBD(Pais); 
        }

        private void comboBoxPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            //select grava o pais seleccionado na combobox
            Paises select = comboBoxPais.SelectedItem as Paises;

            //mostra nas labels os dados do pais que estiver seleccionado 
            labelCapital.Text = select.capital;
            labelRegiao.Text = select.region;
            labelSubRegiao.Text = select.subregion;
            labelPopulacao.Text = select.population.ToString();
            labelArea.Text = select.area;


            {
                try
                {
                    //adicionar os dominios do pais seleccionado na listview
                    listViewDominio.Items.Clear();
                    foreach (var a in select.topLevelDomain)
                    {
                        ListViewItem item1;


                        item1 = listViewDominio.Items.Add(a.ToString());

                    }
                }
                catch (Exception a)
                {
                    dialogService.ShowMessage("Erro", a.Message);

                }
                try
                {
                    //cria colunas na listview moeda
                    listViewMoedas.Columns.Add("Código");
                    listViewMoedas.Columns.Add("Simbolo");
                    listViewMoedas.Columns.Add("Nome");
                    listViewMoedas.Items.Clear();

                    //adicionar as moedas do pais seleccionado na listview
                    foreach (Currency b in select.currencies)
                    {
                        ListViewItem item2;

                        item2 = listViewMoedas.Items.Add(b.code);
                        item2.SubItems.Add(b.symbol);
                        item2.SubItems.Add(b.name);
                    }
                }
                catch (Exception b)
                {
                    dialogService.ShowMessage("Erro", b.Message);

                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        //mostra os dados do programa "ABOUT"
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Luís Nunes | Versão 2.0 | 09-12-2017");
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Luís Nunes | Versão 2.0 | 10-12-2017");
        }
    }
    }
